-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2020 at 03:27 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proyectocnjx`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `comment` char(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` char(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `username`) VALUES
(5, 'Si', 'Xabier'),
(6, 'Pues opino que es una pagina muy elegante', 'Jefe');

-- --------------------------------------------------------

--
-- Table structure for table `infos`
--

CREATE TABLE `infos` (
  `id` int(11) NOT NULL,
  `title` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `info` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `infos`
--

INSERT INTO `infos` (`id`, `title`, `info`) VALUES
(2, '¿Que es Folding@Home?', 'Para poder combatir las diferentes enfermedades, los científicos necesitan entender el funcionamiento de la proteínas y la evolución de estas. Para ello, se requiere una gran cantidad de ordenadores que hagan cálculos y pruebas que ayuden a encontrar una cura para la enfermedad en el menor tiempo posible. \r\nAsí se creó el proyecto Folding@Home. Gracias al software que utilizan, permite que, con la ayuda de gente de todo el mundo que ofrece parte de los recursos de sus ordenadores, se agilice la búsqueda de estas curas.\r\n¿Cómo funciona?\r\nFolding@home es un proyecto donde los ordenadores están unidos mediante internet “computación distribuida”.\r\nComputación distribuida: Es un método donde se sustituye un superordenador mediante varios ordenadores conectados entre ellos, es un método virtual, como por ejemplo: varios ordenadores estando conectados a internet.\r\nEl proyecto Folding@home reúne a ciudadanos que se ofrecen como voluntarios a dar una parte que no se utiliza de sus ordenadores personales.\r\nGracias a que los voluntarios dejan parte de sus ordenadores, ayudan a los científicos a entender mejor la biología ya que ayuda a calcular o resolver curas gracias a que no es un ordenador quien lo procesa sino varios ordenadores.\r\n'),
(3, '¿Por qué nos necesitan?', 'Folding@home es un proyecto centrado en la investigación de enfermedades. ¡Los problemas que estamos resolviendo requieren muchos cálculos informáticos y necesitamos su ayuda para encontrar las curas!\r\n'),
(4, '¿Como ayudamos?', '1.-Instalación del programa\r\n“El software Folding@home le permite compartir su poder informático no utilizado con nosotros, para que podamos investigar aún más curas potenciales.”\r\nColaborando en este proyecto, compartes una parte de los recursos de tu ordenador que normalmente no se usa, para ayudar a los científicos a hacer cálculos y buscar curas mucho más rápido.\r\nEl software Folding@home se ejecuta mientras haces tus cosas en el ordenador.\r\n\r\nMientras tu continuas con tus actividades diarias, este programa instalado en tu ordenador, estará trabajando para ayudar a encontrar curas para las enfermedades, en nuestro caso el Covid 19.\r\n2.- Donaciones\r\nAdemás, también puede donar fondos que nos permitirán continuar (nuestro trabajo en los campos del plegamiento de proteínas) con las investigaciones, como por ejemplo: \r\nEl desarrollo de fármacos y “en la investigación de los sistemas moleculares.”\r\nMANUAL DE USUARIO\r\nPara poder colaborar en este proyecto, es necesario instalarse un programa en su ordenador. \r\nPara ello, deberá seguir los siguientes pasos:\r\n1.- Primero de todo, es necesario saber con que sistema operativo trabaja su ordenador, para poder instalar el programa correcto. (Explicación de como saber el sistema operativo). Disponible para Windows, Linux, Mac\r\n2.- Como hacer la instalacion(paso a paso)\r\n3.- Una vez instalado, registrarse como Nombre_4vientosfolders.\r\n4.- Indicar que  el TEAM ID es 244686, ya que este trabaja exclusivamente en la investigación del Covid 19.\r\n\r\nCOVID 19\r\nCoronavirus: qué estamos haciendo y cómo puede ayudar\r\nPara ayudar a combatir el coronavirus, los científicos necesitan entender el funcionamiento de la proteínas y la evolución del Covid. Para ello necesitan una gran cantidad de ordenadores que hagan cálculos y pruebas que ayuden a encontrar una cura para la enfermedad, en el menor tiempo posible. Para ello,  Aquí es donde nosotros desde nuestras casa, podemos aportar nuestro “granito de arena”. \r\n\r\n“Existen muchos métodos experimentales para determinar las estructuras proteicas. Aunque extremadamente potente, sólo revelan una sola instantánea de la forma habitual de una proteína. Pero las proteínas tienen muchas partes móviles, así que realmente queremos ver la proteína en acción. Las estructuras que no podemos ver experimentalmente pueden ser la clave para descubrir una nueva terapia.”\r\n“Usando el fútbol como analogía para la situación experimental, es como si sólo pudieras ver a los jugadores alineados para el instante (el único arreglo en el que los jugadores pasan la mayor parte del tiempo) y estaban ciegos al resto del juego.”\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` char(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(15) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`) VALUES
('Jefe', 'jefe'),
('Julen', 'Si'),
('Xabier', 'Martin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `infos`
--
ALTER TABLE `infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `infos`
--
ALTER TABLE `infos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
