<?php

//dirname(__FILE__) Es el directorio del archivo actual
require_once(dirname(__FILE__) . '/../conf/PersistentManager.php');

class commentDAO {

    //Se define una constante con el nombre de la tabla
    const COMMENT_TABLE = 'comments';

    //Conexión a BD
    private $conn = null;

    //Constructor de la clase
    public function __construct() {
        $this->conn = PersistentManager::getInstance()->get_connection();
    }

    public function selectAll() {
        $query = "SELECT * FROM " . commentDAO::COMMENT_TABLE;
        $result = mysqli_query($this->conn, $query);
        $comments = array();
        while ($proyectocnjx = mysqli_fetch_array($result)) {

            $comment = new Comment();
            $comment->setId($proyectocnjx["id"]);
            $comment->setComment($proyectocnjx["comment"]);
            $comment->setUsername($proyectocnjx["username"]);
            
            
            array_push($comments, $comment);
        }
        return $comments;
    }

    public function insert($comment) {
        $query = "INSERT INTO " . commentDAO::COMMENT_TABLE .
                " (comment, username) VALUES(?,?)";
        $stmt = mysqli_prepare($this->conn, $query);
        $commente = $comment->getComment();
        $username = $comment->getUsername();
      
        mysqli_stmt_bind_param($stmt, 'ss', $commente, $username);
        return $stmt->execute();
    }

    public function selectById($id) {
        $query = "SELECT comment, username FROM " . commentDAO::COMMENT_TABLE . " WHERE id=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $comment, $username);

        $commente = new Comment();
        while (mysqli_stmt_fetch($stmt)) {
            $commente->setId($id);
            $commente->setComment($comment);
            $commente->setUsername($username);
           
       }

        return $commente;
    }

    public function update($commente) {
        $query = "UPDATE " . commentDAO::COMMENT_TABLE .
                " SET comment=?, username=?"
                . " WHERE id=?";
        $stmt = mysqli_prepare($this->conn, $query);
        $comment = $commente->getComment();
        $username= $commente->getUsername();
       
        $id = $commente->getId();
        mysqli_stmt_bind_param($stmt, 'ssi', $comment, $username, $id);
        return $stmt->execute();
    }
    
    public function delete($id) {
        $query = "DELETE FROM " . commentDAO::COMMENT_TABLE . " WHERE id=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        return $stmt->execute();
    }

        
}

?>
