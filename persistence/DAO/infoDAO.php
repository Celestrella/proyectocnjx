<?php

//dirname(__FILE__) Es el directorio del archivo actual
require_once(dirname(__FILE__) . '/../conf/PersistentManager.php');

class infoDAO {

    //Se define una constante con el nombre de la tabla
    const INFO_TABLE = 'infos';

    //Conexión a BD
    private $conn = null;

    //Constructor de la clase
    public function __construct() {
        $this->conn = PersistentManager::getInstance()->get_connection();
    }

    public function selectAll() {
        $query = "SELECT * FROM " . infoDAO::INFO_TABLE;
        $result = mysqli_query($this->conn, $query);
        $infos = array();
        while ($proyectocnjx = mysqli_fetch_array($result)) {

            $infor = new Info();
            $infor->setId($proyectocnjx["id"]);
            $infor->setTitle($proyectocnjx["title"]);
            $infor->setInfo($proyectocnjx["info"]);
            
            
            array_push($infos, $infor);
        }
        return $infos;
    }

    public function insert($infor) {
        $query = "INSERT INTO " . infoDAO::INFO_TABLE .
                " (title, info) VALUES(?,?)";
        $stmt = mysqli_prepare($this->conn, $query);
        $title = $infor->getTitle();
        $info = $infor->getInfo();
      
        mysqli_stmt_bind_param($stmt, 'ss', $title, $info);
        return $stmt->execute();
    }

    public function selectById($id) {
        $query = "SELECT title, info FROM " . infoDAO::INFO_TABLE . " WHERE id=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $title, $info);

        $infor = new Info();
        while (mysqli_stmt_fetch($stmt)) {
            $infor->setId($id);
            $infor->setTitle($title);
            $infor->setInfo($info);
           
       }

        return $infor;
    }

    public function update($infor) {
        $query = "UPDATE " . infoDAO::INFO_TABLE .
                " SET title=?, info=?"
                . " WHERE id=?";
        $stmt = mysqli_prepare($this->conn, $query);
        $title = $infor->getTitle();
        $info= $infor->getInfo();
       
        $id = $infor->getId();
        mysqli_stmt_bind_param($stmt, 'ssi', $title, $info, $id);
        return $stmt->execute();
    }
    
    public function delete($id) {
        $query = "DELETE FROM " . infoDAO::INFO_TABLE . " WHERE id=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        return $stmt->execute();
    }

        
}

?>
