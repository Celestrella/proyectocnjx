<?php

//dirname(__FILE__) Es el directorio del archivo actual
require_once(dirname(__FILE__) . '..\..\conf\PersistentManager.php');

class UserDAO {

    //Se define una constante con el nombre de la tabla
    const USER_TABLE = 'users';

    //Conexión a BD
    private $conn = null;

    //Constructor de la clase
    public function __construct() {
        $this->conn = PersistentManager::getInstance()->get_connection();
    }

    public function selectAll() {
        $query = "SELECT * FROM " . UserDAO::USER_TABLE;
        $result = mysqli_query($this->conn, $query);
        $users= array();
        while ($userBD = mysqli_fetch_array($result)) {

            $user = new User();
           
            $user->setUsername($userBD["username"]);
            $user->setPassword($userBD["password"]);
            
            array_push($users, $user);
        }
        return $users;
    }

    public function insert($user) {
        $query = "INSERT INTO " . UserDAO::USER_TABLE .
                " (username, password) VALUES(?,?)";
        $stmt = mysqli_prepare($this->conn, $query);
        $username = $user->getUsername();
        $password = $user->getPassword();
        
        mysqli_stmt_bind_param($stmt, 'ss', $username, $password);
        return $stmt->execute();
    }

     public function check($user) {
        $query = "SELECT password FROM " . UserDAO::USER_TABLE . " WHERE username=?";
        $stmt = mysqli_prepare($this->conn, $query);
        $auxName = $user->getUsername();
       
                 
        mysqli_stmt_bind_param($stmt, 's', $auxName);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt); 
        
        $rows = mysqli_stmt_num_rows($stmt);
        
        if($rows>0)  {  
            mysqli_stmt_bind_result($stmt,$password);
            while (mysqli_stmt_fetch($stmt)) {
           
         
            $contrasena=$password;
            if($contrasena==$user->getPassword()){
                return true;
            }else{
                 return false;
            }
           
            }
        }
        else{
            return false;
        }
    }
    
    public function checkNew($user) {
        $query = "SELECT * FROM " . UserDAO::USER_TABLE . " WHERE username=?";
        $stmt = mysqli_prepare($this->conn, $query);
        $auxName = $user->getUsername();
       
                 
        mysqli_stmt_bind_param($stmt, 's', $auxName);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt); 
        
        $rows = mysqli_stmt_num_rows($stmt);
        
        if($rows>0)  {  
           return true;
                
        }
        else{
            return false;
        }
    }
    
    
    public function selectByUsername($username) {
        $query = "SELECT username, password FROM " . UserDAO::USER_TABLE . " WHERE username=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 's', $username);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $username, $password);

        $user = new User();
        while (mysqli_stmt_fetch($stmt)) {
            $user->setUsername($name);
            $user->setPassword($password);
       }

        return $user;
    }
    /*
    public function update($user) {
        $query = "UPDATE " . UserDAO::USER_TABLE .
                " SET username=?, , password=?"
                . " WHERE username=?";
        $stmt = mysqli_prepare($this->conn, $query);
        $email = $user->getName();
        $password= $user->getPassword();
        $id = $user->getIdUser();
        mysqli_stmt_bind_param($stmt, 'sssi', $name, $password, $id);
        return $stmt->execute();
    }
    */
    public function delete($username) {
        $query = "DELETE FROM " . UserDAO::USER_TABLE . " WHERE username =?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 's', $username);
        return $stmt->execute();
    }

        
}

?>
