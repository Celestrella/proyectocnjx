
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>CNJX - Videos</title>

        <!-- Bootstrap Core CSS -->
         <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../../../assets/css/style.css">

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
      



    </head>

    <body>

        <header class="header">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand"  style="color:white">Vídeos</a>
                <a class="navbar-brand" href="https://twitter.com/PCnjx" target="_blank"> <img src="../../../assets/img/Twitter.png" height="30" width="30" alt="" ></a>
                <a class="navbar-brand" href="https://www.instagram.com/proyectocnjx/" target="_blank"> <img src="../../../assets/img/Instagram.png" height="30" width="30" alt="" ></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav m-auto">.
                         <a class="navbar-brand" href="../../../index.php"> <img src="../../../assets/img/Celula.png" height="30" width="30" alt="" ></a>
                       
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../../views/comments/comments.php">Comentarios</a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../../views/infos/infos.php">Información</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="videos.php">Vídeos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../../views/stats/stats.php">Estadísticas</a>
                        </li>
                       <!--<li class="nav-item"> -->
                        <!--<a class="nav-link disabled" href="#">Disabled</a> -->
                        <!--</li> -->
                            
                      
                    </ul>
                    
                </div>
            </nav>
        </header>

        <!-- /.row -->
       
        <h3 class="blanco centrartexto">VIDEO DE PRESENTACIÓN</h3>
        <video controls class="videopequeno"><source src="../../../assets/vid/Videoconferencia.mp4" type="video/mp4" /></video>
        <p class="letratexto centrartexto blanco">Videoconferencia realizada con Maialen Pisón con los alumnos de 4vientos para explicar los puntos importantes del proyecto <br> y preguntas interesantes acerca del mismo.</p>
        
        <h3 class="blanco centrartexto">MANUAL DE INSTALACIÓN</h3>
        <video controls class="videopequeno"><source src="../../../assets/vid/InstalacionBasica.mp4" type="video/mp4" /></video>
        <p class="letratexto centrartexto blanco">Este video explica paso a paso como instalarse el ejecutable de Folding@Home y poder participar en el proyecto.</p>
        
        <a class="letratexto btn btn-info" style="display: block;margin:0 auto; width: 500px" href="../../../assets/downloads/fah-installer_7.6.13_x86.exe">Enlace de descarga del ejecutable para Windows</a>
        <br>
     
        <a class="letratexto btn btn-info" target="_blank" style="display: block;margin:0 auto; width: 500px" href="https://foldingathome.org/alternative-downloads/">Enlace a descarga alternativa( Mac y otros)</a>
        <br>
        <br>


        
    <!-- /.container -->

    <!-- jQuery -->
  <script src="../../../assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../assets/js/bootstrap.min.js"></script>
</body>

</html>