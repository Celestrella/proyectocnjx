<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>CNJX - Añadir Comentarios</title>

        <!-- Bootstrap Core CSS -->
          <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../../../assets/css/style.css">

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


    </head>
    <body>
        <!-- Navigation -->
        <header class="header">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand"  style="color:white" href="../../index.php">Añadir comentario</a>
                <a class="navbar-brand" href="https://twitter.com/PCnjx" target="_blank"> <img src="../../../assets/img/Twitter.png" height="30" width="30" alt="" ></a>
                <a class="navbar-brand" href="https://www.instagram.com/proyectocnjx/" target="_blank"> <img src="../../../assets/img/Instagram.png" height="30" width="30" alt="" ></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav m-auto">.
                        <a class="navbar-brand" href="../../../index.php"> <img src="../../../assets/img/Celula.png" height="30" width="30" alt="" ></a>
                       
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="comments.php">Comentarios</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../infos/infos.php">Información</a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../videos/videos.php">Vídeos</a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../stats/stats.php">Estadísticas</a>
                        </li>
                       
                    </ul>

                </div>
            </nav>

           

        </header> 
    
    <!-- Page Content -->
   <div class="container">
            <form class="form-horizontal" method="post" action="../../controllers/comments/insertController.php">
                <div class="form-group">
                    <label for="username" class="col-sm-2 control-label blanco">Nombre de usuario</label>
                  
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="username" id="username" placeholder="Nombre de usuario" readonly="readonly" value="<?php  echo $_SESSION['user'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="comment" class="col-sm-2 control-label blanco">Comentario</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="comment" name="comment" placeholder="Comentario" >
                    </div>
                </div>
               
                
                
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Añadir</button>
                    </div>
                </div>
            </form>

            <!-- Footer -->
           

        </div>

        <!-- Footer -->


    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="../../assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../assets/js/bootstrap.min.js"></script>
</body>

</html>


