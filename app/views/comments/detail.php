<?php
//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '../../../../persistence/DAO/commentDAO.php');
require_once(dirname(__FILE__) . '../../../../app/models/Comment.php');


//Compruebo que me llega por GET el parámetro
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    //Creamos un objeto CommentDAO para hacer las llamadas a la BD
    $commentDAO = new commentDAO();
    $Comment = $commentDAO->selectById($id);
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Detalles del comentario</title>

        <!-- Bootstrap Core CSS -->
        <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../../../assets/css/style.css">

    </head>
    <body>
         <!-- Navigation -->
        <header class="header">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand"  style="color:white" href="../../index.php">Paradigm</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav m-auto">.
                        <a class="navbar-brand" href="../../../index.php"> <img src="../../../assets/img/Celula.png" height="30" width="30" alt="" ></a>
                       
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="comments.php">Comentarios</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../infos/infos.php">Información</a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../videos/videos.php">Vídeos</a>
                        </li>
                       
                    </ul>

                </div>
            </nav>

           

        </header> 
     
        <!-- Page Content -->
        <div class="container">

            
                <h2 class="card-title"> <?php echo $Comment->getUsername() ?></h2>
                <p class=" card-text description"> <?php echo  $Comment->getComment()?></p>                  
             </div>
              <div  class=" btn-group card-footer" role="group">
              <a type="button" class="btn btn-success" href="edit.php?id=<?php echo $Comment->getId() ?>">Modificar</a> 
                <a type="button" class="btn btn-danger" href="../controllers/comments/deleteController.php?id=<?php  echo $Comment->getId()?>">Borrar</a> 
             </div>
         </div>
            
          

            <!-- Footer -->
          

        </div>
        <!-- /.container -->

        <!-- jQuery -->
        <script src="../../../assets/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../../../assets/js/bootstrap.min.js"></script>
    </body>

</html>


