<?php
//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '../../../../persistence/DAO/commentDAO.php');
require_once(dirname(__FILE__) . '../../../../app/models/Comment.php');


//Compruebo que me llega por GET el parámetro
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    
    $commentDAO = new commentDAO();
    $Comment = $commentDAO->selectById($id);
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Gestión de comentarios</title>

        <!-- Bootstrap Core CSS -->
        <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../../../assets/css/style.css">


    </head>
    <body>
         <!-- Navigation -->
       <header class="header">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand"  style="color:white" href="../../index.php">Editar comentario</a>
                <a class="navbar-brand" href="https://twitter.com/PCnjx" target="_blank"> <img src="../../../assets/img/Twitter.png" height="30" width="30" alt="" ></a>
                <a class="navbar-brand" href="https://www.instagram.com/proyectocnjx/" target="_blank"> <img src="../../../assets/img/Instagram.png" height="30" width="30" alt="" ></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav m-auto">.
                        <a class="navbar-brand" href="../../../index.php"> <img src="../../../assets/img/Celula.png" height="30" width="30" alt="" ></a>
                       
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="comments.php">Comentarios</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../infos/infos.php">Información</a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../videos/videos.php">Vídeos</a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../stats/stats.php">Estadísticas</a>
                        </li>
                       
                    </ul>

                </div>
            </nav>

           

        </header> 

        <!-- Page Content -->
        <div class="container">
            <form class="form-horizontal" method="post" action="../../controllers/comments/editController.php">
                <div class="form-group">
                    <label for="username" class="col-sm-2 control-label blanco">Username</label>
                  
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="username" id="username" placeholder="Username" readonly="readonly" value="<?php echo $Comment->getUsername(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="comment" class="col-sm-2 control-label blanco">Comment</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="comment" name="comment" placeholder="Comment" value="<?php echo $Comment->getComment(); ?>">
                    </div>
                </div>
               
                
                <input type="hidden" name="id" value="<?php echo $Comment->getId(); ?>">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Edit</button>
                    </div>
                </div>
            </form>

            <!-- Footer -->
           

        </div>
        <!-- /.container -->

        <!-- jQuery -->
        <script src="../../../assets/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../../../assets/js/bootstrap.min.js"></script>
    </body>

</html>


