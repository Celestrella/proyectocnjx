<?php
//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
session_start();
require_once(dirname(__FILE__) . '../../../controllers/comments/commentsController.php');

//Recupero de la BD todos los comentarios a través del controlador
$comments = commentAction();
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>CNJX - Comentarios</title>

        <!-- Bootstrap Core CSS -->
         <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../../../assets/css/style.css">

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
      



    </head>

    <body>

        <header class="header">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand"  style="color:white">Comentarios</a>
                <a class="navbar-brand" href="https://twitter.com/PCnjx" target="_blank"> <img src="../../../assets/img/Twitter.png" height="30" width="30" alt="" ></a>
                <a class="navbar-brand" href="https://www.instagram.com/proyectocnjx/" target="_blank"> <img src="../../../assets/img/Instagram.png" height="30" width="30" alt="" ></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav m-auto">.
                         <a class="navbar-brand" href="../../../index.php"> <img src="../../../assets/img/Celula.png" height="30" width="30" alt="" ></a>
                       
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="comments.php">Comentarios</a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../../views/infos/infos.php">Información</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../../views/videos/videos.php">Vídeos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../../views/stats/stats.php">Estadísticas</a>
                        </li>
                       <!--<li class="nav-item"> -->
                        <!--<a class="nav-link disabled" href="#">Disabled</a> -->
                        <!--</li> -->
                            
                      
                    </ul>
                    
                </div>
            </nav>
        </header>
        
        <!-- /.row -->
           <?php
           if(isset($_SESSION["user"])){
               
               ?>
                <a class="btn btn-info m-r" href="insert.php">Deja tu comentario!</a>
                <?php
               
               }
           ?>
     

        
        <div class="container centrartexto ">
        <!-- Content Row -->
        <?php for ($i = 0; $i < sizeof($comments); $i+= 3) { ?>
            <!--   <div class="card-group">   -->
            <div class="row "> 
                <?php
                for ($j = $i; $j < ($i + 3); $j++) {
                    if (isset($comments[$j])) {

                        echo $comments[$j]->comment2HTML();
                    }
                }
                ?>
            </div> 
            <!-- /.row -->
        <?php } ?>

        <!-- Footer -->


    </div>
    <!-- /.container -->

    <!-- jQuery -->
  <script src="../../../assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../assets/js/bootstrap.min.js"></script>
</body>

</html>