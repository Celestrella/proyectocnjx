<?php
    session_start();
    
    if(isset($_SESSION["error"])){
        $error=$_SESSION["error"];
    }
     
     
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>CNJX - Registro</title>

        <!-- Bootstrap Core CSS -->
        <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../../../assets/css/style.css">


    </head>
    <body>
        <!-- Navigation -->
        <header class="header">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand"  style="color:white" href="../../../index.php">CNJX</a>
                <a class="navbar-brand" href="https://twitter.com/PCnjx" target="_blank"> <img src="../../../assets/img/Twitter.png" height="30" width="30" alt="" ></a>
                <a class="navbar-brand" href="https://www.instagram.com/proyectocnjx/" target="_blank"> <img src="../../../assets/img/Instagram.png" height="30" width="30" alt="" ></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav m-auto">.
                         <a class="navbar-brand" href="../../../index.php"> <img src="../../../assets/img/Celula.png" height="30" width="30" alt="" ></a>
                       
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../../views/comments/comments.php">Comentarios</a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../../views/infos/infos.php">Información</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../../views/videos/videos.php">Vídeos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../../views/stats/stats.php">Estadísticas</a>
                        </li>
                       <!--<li class="nav-item"> -->
                        <!--<a class="nav-link disabled" href="#">Disabled</a> -->
                        <!--</li> -->
                            
                      
                    </ul>

                </div>
            </nav>

           

        </header>
    
    <!-- Page Content -->
    <div class="container">
            <div class="row m-y-2">
                <div class="col-lg-8 push-lg-4 centrar">
                    <form class="form-horizontal " role="form" method="POST" action="../../controllers/user/insertController.php">
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <h2 class="blanco">Por favor registrate</h2>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group has-danger">
                                    <label class="sr-only" for="username">Nombre de usuario:</label>
                                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                        <div class="input-group-addon"></div>
                                        <input type="text" name="username" class="form-control" id="username"
                                               placeholder="Nombre de usuario" required autofocus>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-control-feedback">
                                    <span class="text-danger align-middle">
                                        <?php
                                        if (isset($error)) {
                                            echo $error;
                                        }
                                        ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="sr-only" for="password">Contraseña:</label>
                                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                        <div class="input-group-addon" ></div>
                                        <input type="password" name="password" class="form-control" id="password"
                                               placeholder="Contraseña" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-control-feedback">
                                    <span class="text-danger align-middle">
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row" >
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-success" value="Register" id="register" name="btnsubmit">Registrarse</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="../../../assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../assets/js/bootstrap.min.js"></script>
</body>

</html>

