<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>CNJX - Estadísticas</title>

        <!-- Bootstrap Core CSS -->
         <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../../../assets/css/style.css">

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
      



    </head>

    <body>

        <header class="header">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand"  style="color:white">Estadísticas</a>
                <a class="navbar-brand" href="https://twitter.com/PCnjx" target="_blank"> <img src="../../../assets/img/Twitter.png" height="30" width="30" alt="" ></a>
                <a class="navbar-brand" href="https://www.instagram.com/proyectocnjx/" target="_blank"> <img src="../../../assets/img/Instagram.png" height="30" width="30" alt="" ></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav m-auto">.
                         <a class="navbar-brand" href="../../../index.php"> <img src="../../../assets/img/Celula.png" height="30" width="30" alt="" ></a>
                       
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../../views/comments/comments.php">Comentarios</a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../../views/infos/infos.php">Información</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="../../views/videos/videos.php">Vídeos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="stats.php">Estadísticas</a>
                        </li>
                       <!--<li class="nav-item"> -->
                        <!--<a class="nav-link disabled" href="#">Disabled</a> -->
                        <!--</li> -->
                            
                      
                    </ul>
                    
                </div>
            </nav>
        </header>

        <!-- /.row -->
       
       <?php
        //$datos = json_decode( file_get_contents('https://stats.foldingathome.org/api/team/244686'), true );
     
        //Guado en una variable el resultado de la api como JSON
        $resulApi = file_get_contents('https://stats.foldingathome.org/api/team/244686');
         //print_r($resulApi);
          echo "<br><br>";
       
          //Buscar pos ini y fin del array de donantes en el JSON
        $posIni = strpos($resulApi, "[");
        $posFin = strpos($resulApi, "]");
        if($posFin != 0){
             $Team = substr($resulApi, $posIni, ($posFin- $posIni +1));
        }
        //Convertir en JSON en un array
        $ArrayTeam = json_decode($Team , true );
        //print_r($ArrayDonantes);
        echo "<br>";
        //Recorro el array mostrando los nombres
        echo "<h3 class='blanco centrartexto letratexto'>PARTICIPANTES DEL PROYECTO CUATROVIENTOS</h3>";
        echo"<br>";
        echo "<div class='container blanco letratexto'>";
        foreach ($ArrayTeam as $value){
           print ($value['name'])." , "  ;
        }
        
        echo "</div>";
        echo"<br>";
        $numeroparticipantes = count($ArrayTeam);
        echo "<div class='container blanco letratexto'>El numero de participantes es: $numeroparticipantes</div>";
        
        $datos = json_decode( file_get_contents('https://stats.foldingathome.org/api/team/244686'), true );
        $cont =0;
   
        foreach ($datos as $obj){
            $cont += 1;
            if ($cont == 3){
               
                $rango = $obj;
                echo "<div class='container blanco letratexto'>La posicion del grupo Cuatrovientos en el top de donaciones es: $rango</div>";
            }
            if ($cont == 4){
                $cantTeams = $obj;
                echo "<div class='container blanco letratexto'>El total de equipos es: $cantTeams</div>";
            }
           
            
        }
                
                
        $resultApi = file_get_contents('https://stats.foldingathome.org/api/donors');
         //print_r($resultApi);
          echo "<br><br>";
       
          //Buscar pos ini y fin del array de donantes en el JSON
        $posInii = strpos($resultApi, "[");
        $posFini = strpos($resultApi, "]");
        if($posFin != 0){
             $Donantes = substr($resultApi, $posInii, ($posFini- $posInii +1));
        }
        //Convertir en JSON en un array
        $ArrayDonantes = json_decode($Donantes , true );
        //print_r($ArrayDonantes);
        echo "<br>";
        //Recorro el array mostrando los nombres
        echo "<h3 class='blanco centrartexto letratexto'>TOP 10 DONANTES</h3>";
        echo"<br>";
        echo "<div class='container blanco letratexto'>";
        $contador=1;
        foreach ($ArrayDonantes as $value){
            if($contador<11){
                print "$contador"." - ".($value['name']) ."<br>" ;
                $contador+=1;
            }else{
                $contador=1;
                exit;
            }
           
        }
        echo "<br>";
        echo "<br>";
        echo "<br>";
        echo "</div>";
        
       
       
        ?>
     
    

        
    <!-- /.container -->

    <!-- jQuery -->
  <script src="../../../assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../assets/js/bootstrap.min.js"></script>
</body>

</html>

