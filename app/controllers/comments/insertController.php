<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../../persistence/DAO/commentDAO.php');
require_once(dirname(__FILE__) . '/../../../app/models/Comment.php');
require_once(dirname(__FILE__) . '/../../../app/models/validations/ValidationsRules.php');



if ($_SERVER["REQUEST_METHOD"] == "POST") {
//Llamo a la función en cuanto se redirija el action a esta página
    createAction();
}

function createAction() {
    
    
    $comment = ValidationsRules::test_input($_POST["comment"]);
    $username = ValidationsRules::test_input($_POST["username"]);
    
    
    $comment = new Comment();
    $comment ->setComment($_POST["comment"]);
    $comment ->setUsername($_POST["username"]);
    

    //Creamos un objeto maskDAO para hacer las llamadas a la BD
    $commentDAO = new commentDAO();
    $commentDAO->insert($comment);
    
    header('Location: ../../views/comments/comments.php');
    
}
?>

