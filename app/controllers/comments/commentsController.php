<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../../persistence/DAO/commentDAO.php');
require_once(dirname(__FILE__) . '/../../../app/models/Comment.php');


function commentAction() {
    $commentDAO = new commentDAO();
    return $commentDAO->selectAll();
}

?>