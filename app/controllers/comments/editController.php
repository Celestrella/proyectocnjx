<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../../persistence/DAO/commentDAO.php');
require_once(dirname(__FILE__) . '/../../../app/models/Comment.php');


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    editAction();
}

function editAction() {
    
    $id = $_POST["id"];
    $comment = $_POST["comment"];
    $username = $_POST["username"];
    

    $commente = new Comment();
    $commente ->setId($id);
    $commente ->setComment($comment);
    $commente ->setUsername($username);
    

    $commentDAO = new commentDAO();
    $commentDAO->update($commente);

    header('Location: ../../views/comments/comments.php');
}

?>

