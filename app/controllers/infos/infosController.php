<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../../persistence/DAO/infoDAO.php');
require_once(dirname(__FILE__) . '/../../../app/models/Info.php');


function infoAction() {
    $infoDAO = new infoDAO();
    return $infoDAO->selectAll();
}

?>