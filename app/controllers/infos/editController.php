<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../../persistence/DAO/infoDAO.php');
require_once(dirname(__FILE__) . '/../../../app/models/Info.php');


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    editAction();
}

function editAction() {
    
    $id = $_POST["id"];
    $title = $_POST["title"];
    $info = $_POST["info"];
    

    $infor = new Info();
    $infor ->setId($id);
    $infor ->setTitle($title);
    $infor ->setInfo($info);
    

    $infoDAO = new infoDAO();
    $infoDAO->update($infor);

    header('Location: ../../../index.php');
}

?>

