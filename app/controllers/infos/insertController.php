<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../../persistence/DAO/infoDAO.php');
require_once(dirname(__FILE__) . '/../../../app/models/Info.php');
require_once(dirname(__FILE__) . '/../../../app/models/validations/ValidationsRules.php');



if ($_SERVER["REQUEST_METHOD"] == "POST") {
//Llamo a la función en cuanto se redirija el action a esta página
    createAction();
}

function createAction() {
    
    
    $title = ValidationsRules::test_input($_POST["title"]);
    $info = ValidationsRules::test_input($_POST["info"]);
    
    
    $infor = new Info();
    $infor ->setTitle($_POST["title"]);
    $infor ->setInfo($_POST["info"]);
    

    //Creamos un objeto infoDAO para hacer las llamadas a la BD
    $infoDAO = new infoDAO();
    $infoDAO->insert($infor);
    
    header('Location: ../../../index.php');
    
}
?>

