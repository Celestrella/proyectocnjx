<?php
//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../../persistence/DAO/UserDAO.php');
require_once(dirname(__FILE__) . '/../../../app/models/User.php');
require_once(dirname(__FILE__) . '/../../../app/models/validations/ValidationsRules.php');
require_once(dirname(__FILE__) . '/../../../utils/SessionUtils.php');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    //Llamo a la función en cuanto se redirige el action a esta página mediante metodo POST
   createAction();
}
// Función encargada de crear nuevos usuarios
function createAction() {
    // Obtención de los valores del formulario y validación
    $username = ValidationsRules::test_input($_POST["username"]);
    $password = ValidationsRules::test_input($_POST["password"]);
    // Creación de objeto auxiliar   
    $user = new User();
    $user->setUsername($username);
    $user->setPassword($password);
    //Creamos un objeto UserDAO para hacer las llamadas a la BD
    $userDAO = new UserDAO();
    if($userDAO->checkNew($user)){
        //$_POST["error"]="Usuario ya registrado";
       //echo "<script>alert('Usuario ya registrado')</script>";
        session_start();
        $_SESSION["error"]="Usuario ya registrado";
       header('Location: ../../views/user/signup.php'); 
    }else{
         $userDAO->insert($user);
         // Establecemos la sesión
    SessionUtils::startSessionIfNotStarted();
    SessionUtils::setSession($user->getUsername());
    header('Location: ../../../index.php');  
    }
   
   
       
    
}

    