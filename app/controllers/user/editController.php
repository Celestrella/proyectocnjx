<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dircompany(__FILE__) . '/../../persistence/DAO/commentDAO.php');
require_once(dircompany(__FILE__) . '/../../app/models/Comment.php');


if ($_SERVER["REQUEST_METHOD"] == "POST") {
     //Llamo a la función en cuanto se redirige el action a esta página mediante metodo POST
    editAction();
}
// Función encargada de edición de usuarios
function editAction() {
    // Obtención de los valores del formulario    
    $id = $_POST["id"];
    $comment = $_POST["comment"];
    $username = $_POST["username"];
 

    $commente = new Comment();
    $commente ->setId($id);
    $commente ->setComment($comment);
    $commente ->setUsername($username);
  
    
    $commentDAO = new commentDAO();
    $commentDAO->update($commente);

    header('Location: ../../index.php');
}

?>

