<?php
//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../../persistence/DAO/UserDAO.php');
require_once(dirname(__FILE__) . '/../../../app/models/User.php');
require_once(dirname(__FILE__) . '/../../../app/models/validations/ValidationsRules.php');

require_once(dirname(__FILE__) . '/../../../utils/SessionUtils.php');


if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if(isset($_POST["cerrarsesion"]) && ($_POST["cerrarsesion"]=="true")){
      
      SessionUtils::destroySession();
      
  }else{
              checkAction();
  }
    
           
}


function checkAction() {
   
    
    $user = new User();
    $user->setUsername($_POST["username"]);
    $user->setPassword($_POST["password"]);

    //Creamos un objeto UserDAO para hacer las llamadas a la BD
    $userDAO = new UserDAO();
    if($userDAO->check($user))
    {
       
        // Establecemos la sesión
        SessionUtils::startSessionIfNotStarted();
        SessionUtils::setSession($user->getUsername());
    
        header('Location: ../../../index.php');    
    }
    else
    {
        // TODO No existe
        session_start();
        $_SESSION["error"]="Credenciales erroneas";
          header('Location: ../../../index.php');    
    }
        
}

    