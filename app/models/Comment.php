<?php

class Comment {

    private $id;
    private $comment;
    private $username;
    

    public function __construct() {
        
    }
    
    

    public function getId() {
        return $this->id;
    }

    public function getComment() {
        return $this->comment;
    }

    public function getUsername() {
        return $this->username;
    }

  

    public function setId($id) {
        $this->id= $id;
    }

    public function setComment($comment) {
        $this->comment = $comment;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    
    
//Función para pintar cada comentario
    function comment2HTML() {
          $result = '</br>';
          $result .= '<div class=" col-md-4 ">';
          $result .= '<div class="card tamanotarjeta">';
          $result .= '<div class="card-body">';
          $result .= '<h2 class="card-title">' . $this->getUsername() . '</h2>';
          $result .= '<p class=" card-text description">' . $this->getComment() . '</p>';
          $result .= '</div>';
          if(isset($_SESSION["user"])){
               if($this->getUsername()==($_SESSION['user'])){
              $result .= ' <div  class=" btn-group card-footer" role="group">';
          $result .= '<a type="button" class="btn btn-success" href="edit.php?id=' . $this->getId() . '">Modificar</a> ';
          $result .= '<a type="button" class="btn btn-danger" href="../../controllers/comments/deleteController.php?id=' . $this->getId() . '">Borrar</a> ';
          $result .= ' </div>';
          }
          }
          $result .= '</div>';
          $result .= '</div>';
          

         
        

        return $result;
    }

}
