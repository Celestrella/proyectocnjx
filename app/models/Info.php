<?php

class Info {

    private $id;
    private $title;
    private $info;
    

    public function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getInfo() {
        return $this->info;
    }

  

    public function setId($id) {
        $this->id= $id;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setInfo($info) {
        $this->info = $info;
    }

    
    
//Función para pintar cada infografía
    function info2HTML() {
          $result = '</br>';
          $result .= '<div class=" col-md-12 ">';
          $result .= '<div class="card margentarjeta">';
          $result .= '<div class="card-body">';
          $result .= '<h2 class="card-title">' . $this->getTitle() . '</h2>';
          $result .= '<p class=" card-text description">' . $this->getInfo() . '</p>';
          $result .= '</div>';
          $result .= '</div>';
          $result .= '</div>';
          
          
          

         
        

        return $result;
    }

}