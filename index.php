<?php
 
    if(isset($_SESSION["error"])){
        $error=$_SESSION["error"];
    }
//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . './utils/SessionUtils.php');


  

?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>CNJX - Inicio</title>

        <!-- Bootstrap Core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="assets/css/style.css">



    </head>

    <body>

        <header class="header">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand"  style="color:white" href="index.php">CNJX</a>
                <a class="navbar-brand" href="https://twitter.com/PCnjx" target="_blank"> <img src="assets/img/Twitter.png" height="30" width="30" alt="" ></a>
                <a class="navbar-brand" href="https://www.instagram.com/proyectocnjx/" target="_blank"> <img src="assets/img/Instagram.png" height="30" width="30" alt="" ></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav m-auto">.
                        <a class="navbar-brand" href="index.php"> <img src="assets/img/Celula.png" height="30" width="30" alt="" ></a>
                        
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="./app/views/comments/comments.php">Comentarios</a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" style="color:white" href="./app/views/infos/infos.php">Información</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:white" href="./app/views/videos/videos.php">Vídeos</a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" style="color:white" href="./app/views/stats/stats.php">Estadísticas</a>
                        </li>
                      
                   
                       <!--<li class="nav-item"> -->
                        <!--<a class="nav-link disabled" href="#">Disabled</a> -->
                        <!--</li> -->
                            
                      
                    </ul>
                 <?php
                if (SessionUtils::loggedIn()){
                    ?>
                    <h5><span class='badge badge-success p-3 '> Has iniciado sesión: <?php  echo $_SESSION['user'] ?></span></h5>
                    <form class="form-inline" role="form" method="POST" class="float-right" action="app/controllers/user/userController.php">
                        <input type="hidden" name="cerrarsesion" value="true"/>
                     <button type="submit" class="btn btn-danger m-1 " value="Login" id="login" name="btnsubmit"> Cerrar sesión</button>
                    </form>
                   <?php 
                }
                 else {
                    ?> <form class="form-inline" role="form" method="POST" class="float-right" action="app/controllers/user/userController.php">
                            <input type="text" name="username" class="form-control m-1" id="username" 
                                   placeholder="Usuario" required autofocus/>
                            <?php
                                        if (isset($error)) {
                                            echo $error;
                                        }
                                        ?>
                            <input type="password" name="password" class="form-control m-1" id="password"
                                   placeholder="Contraseña" required/>
                        <button type="submit" class="btn btn-success m-1" value="Login" id="login" name="btnsubmit"> Acceder</button>
                        </form>
                    <form form class="form-inline" role="form" method="POST" class="float-right" action="app/views/user/signup.php">
                        <button type="submit" class="btn btn-info m-1" value="Login" id="login" name="btnsubmit"> Registrarse</button>
                    </form>
                    
               <?php }
                ?>
                      
                    
                </div>
            </nav>
        </header>
        <div class="jumbotron">
            <video id="video-background" preload muted autoplay loop>
                <source src="assets/vid/Virus.mp4" type="video/mp4">
                <source src="assets/vid/Virus.mp4" type="video/ogg">
            </video>
            
        </div>

        <!-- Page Content -->
        <div class="container">

            <!-- Heading Row -->
            <div class="row">

                <!-- /.col-md-8 -->
                <div class="col-md-4">
                    <h1 class="blanco">Visita la página Folding@Home para leer mas información</h1>
                    <a class="btn btn-primary btn-lg" href="https://foldingathome.org/" target="_blank">Descubre más aquí!</a> 
                </div>
                <div class="col-md-4">
                    
                </div>
                
                <div class="col-md-4">
                    <h1 class="blanco">Redes Sociales de Folding@Home</h1>
                    <a class="navbar-brand" href="https://twitter.com/foldingathome" target="_blank"><img src="assets/img/Twitter.png" height="30" width="30" alt="" ></a><a class="navbar-brand" href="https://www.facebook.com/Foldinghome-136059519794607" target="_blank"><img src="assets/img/Facebook.png" height="30" width="30" alt="" ></a>
                    
                    
                </div>
            </div>
        </div>
  

        <!-- Footer -->


    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>

</body>

</html>